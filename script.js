var dailyGraph, hourlyGraph, weekdayGraph, yearlyGraph;


$(document).ready(function(){

    new JSScroll();

    master.players.load();
    ko.applyBindings(master);

    
    dailyGraph   = new CanvasJS.Chart("dailyGraph", dailyGraphOptions);
    hourlyGraph  = new CanvasJS.Chart("hourlyGraph", hourlyGraphOptions);
    weekdayGraph = new CanvasJS.Chart("weekdayGraph", weekdayGraphOptions);
    yearlyGraph  = new CanvasJS.Chart("yearlyGraph", yearlyGraphOptions);


})

var master = {
    players: new PlayerModel(),
    overall: new OverallModel()
}


master.players.selectedSteamid.subscribe(master.overall.load);
master.players.selectedSteamid.subscribe(loadDailyGraph);
master.players.selectedSteamid.subscribe(loadHourlyGraph);
master.players.selectedSteamid.subscribe(loadWeekdayGraph);
master.players.selectedSteamid.subscribe(loadYearlyGraph);
