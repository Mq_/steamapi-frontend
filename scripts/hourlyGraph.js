hourlyGraphOptions = {
    title: {
        text: "Stündliche Spielzeit",
        fontSize: 14
    },
    axisX: {
        title: "Stunde",
        minimum: -0.5,
        maximum: 23.5,
        interval: 1,
        labelFontSize: 10,
        titleFontSize: 12
    },
    axisY: {
        title: "Spielzeit",
        valueFormatString: "0.0 %",
        minimum: 0,
        labelFontSize: 10,
        titleFontSize: 12
    },
    data: [
    {
        type: "column",
        color: "#123456",
        xValueFormatString: "Stunde #",
        yValueFormatString: "#0.0 %"
    }]
}


function loadHourlyGraph(steamid) {
    if (steamid) {
        $.getJSON("data.php", {
                type: "hourly",
                steamid: steamid
            },
            function(data) {
                hourlyGraph.options.data[0].dataPoints = data["hourly"].map(createMapFunction(data["sum"]));
                hourlyGraph.render();

            }
        )
    }
}

function createMapFunction(sum) {
    return function(data) {
        return {
            x: data["hour"],
            y: data["playtime"]/sum,
            toolTipContent: "{x}: {y} | " + Math.round(data["playtime"]/6)/10 + " h"
        };
    }
}