weekdayGraphOptions = {
    title: {
        text: "Wochentägliche Spielzeit",
        fontSize: 14
    },
    axisX: {
        title: "Wochentag",
        valueFormatString: "DDD",
        interval: 1,
        intervalType: "day",
        minimum: new Date(2000,1,-1,12,0,0,0),
        maximum: new Date(2000,1, 6,12,0,0,0),
        labelFontSize: 10,
        titleFontSize: 12
    },
    axisY: {
        title: "Spielzeit",
        valueFormatString: "0.0 %",
        minimum: 0,
        labelFontSize: 10,
        titleFontSize: 12
    },
    data: [
    {
        type: "column",
        color: "#123456",
        xValueType: "dateTime",
        yValueFormatString: "#0.0 %"
    }]
}


function loadWeekdayGraph(steamid) {
    if (steamid) {
        $.getJSON("data.php", {
                type: "weekday",
                steamid: steamid
            },
            function(data) {
                weekdayGraph.options.data[0].dataPoints = data["weekday"].map(createWeekdayFunction(data["sum"]));
                weekdayGraph.render();

            }
        )
    }
}

function createWeekdayFunction(sum) {
    return function(data) {
        //return {x: data["weekday"], y: data["playtime"]/sum};
        return {
            x: new Date(2000, 1, data["weekday"], 0,0,0,0),
            y: data["playtime"]/sum,
            name: data["playtime"],
            toolTipContent: "{x}: {y} | " + Math.round(data["playtime"]/60) + " h"
        };
    }
}