function OverallModel() {

    var self = this;
    self.games = ko.observableArray();
    self.playtime = ko.observable(0);

    self.load = function(steamid) {
        if (!steamid){
            return;
        }
        $.getJSON("data.php", {
            type: "overall",
            steamid: steamid
        },
        function(data) {
            self.games(data.games);
            self.playtime(data.sum);
        });
    }

    self.constructURL = function(appid, hash) {
        return "http://media.steampowered.com/steamcommunity/public/images/apps/" + appid + "/" + hash + ".jpg";
    }

    self.constructTime = function(minutes) {
        // console.log(minutes);
        minutes = parseInt(minutes);
        var hours = Math.floor(minutes / 60);
        var result = hours + " h ";
        if (minutes % 60 < 10){
            result += "0";
        }
        result += minutes % 60 + " min";
        return result;
    }

}