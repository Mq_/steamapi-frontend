<?php

// error_reporting(E_ALL);
// ini_set('display_errors', 'On');


header("Content-Type: application/json; charset=UTF-8");
$conn = @mysql_connect("192.168.0.3", "steamapi");
@mysql_select_db("steam");

$output = array();

if (isset($_GET['type']) && $_GET['type'] == "players") {
    $result = mysql_query("SELECT steamid, name FROM player");
    while(($row = mysql_fetch_assoc($result)) != null){
        $output["players"][] = $row;
    }
}


if (isset($_GET['type']) && isset($_GET['steamid'])) {

    $steamid = preg_replace("/[^0-9]/", "", $_GET['steamid']);

    switch ($_GET['type']) {
        case "overall":
            $query = sprintf("SELECT
                        apps.appid,
                        name,
                        MAX(playtime) as playtime,
                        img_icon_url,
                        img_logo_url
                    FROM apps, playtime
                    WHERE
                        apps.appid = playtime.appid AND
                        steamid = %s
                    GROUP BY playtime.appid
                    ORDER BY playtime DESC", $steamid);
            $result = mysql_query($query);
            $output["games"] = array();
            $rank = 1;
            $sum = 0;
            while(($row = mysql_fetch_assoc($result)) != null){
                $row["rank"] = $rank++;
                $output["games"][] = $row;
                $sum += intval($row["playtime"]);
            }
            $output["sum"] = $sum;
            break;
        case "daily":
            $query = sprintf('SELECT
                DATE(time - INTERVAL 1 HOUR) as date,
                appid,
                MAX(playtime) - (SELECT
                    MAX(playtime) FROM playtime
                    WHERE
                        steamid = %1$s AND
                        appid = p.appid AND
                        DATE(time - INTERVAL 1 hour) < date
                ) as playtime
                FROM playtime p
                WHERE
                    steamid = %1$s AND
                    DATE(time) > CURDATE() - INTERVAL 30 DAY
                GROUP BY date, appid', $steamid);
            $result = mysql_query($query);
            $output["games"] = array();
            //$output["query"] = preg_replace("/\s+/", " ", str_replace("\n", "", $query));
            $sum = array();
            while (($row = mysql_fetch_assoc($result)) != null) {
                if ($row["playtime"] != null) {
                    $output["games"][$row["appid"]][] = array(
                        "date" => $row["date"],
                        "playtime" => intval($row["playtime"])
                    );
                    if (isset($sum[$row["date"]])) {
                        $sum[$row["date"]] += intval($row["playtime"]);
                    } else {
                        $sum[$row["date"]] = intval($row["playtime"]);
                    }
                }
            }

            ksort($sum);
            foreach ($sum as $date => $value) {
                $output["sum"][] = array(
                    "date" => $date,
                    "playtime" => $value
                );
            }

        break;
        case "hourly":
            $query = sprintf('SELECT
                hour, SUM(playtime) as playtime
                FROM
                    hourly_playtime
                WHERE
                    steamid = %1$s
                GROUP BY
                    hour
                ORDER BY
                    hour', $steamid);
            $result = mysql_query($query);
            $output["hourly"] = array();
            $sum = 0;
            while (($row = mysql_fetch_assoc($result)) != null) {
                $sum += $row["playtime"];
                $output["hourly"][] = array(
                    "hour" => $row["hour"],
                    "playtime" => intval($row["playtime"])
                );
            }
            $output["sum"] = $sum;
        break;
        case "weekday":
            $query = sprintf('SELECT
                WEEKDAY(time - INTERVAL 1 HOUR) as day, SUM(playtime) as playtime
                FROM
                    hourly_playtime
                WHERE
                    steamid = %1$s
                GROUP BY
                    WEEKDAY(time - INTERVAL 1 HOUR)', $steamid);
            $result = mysql_query($query);
            $sum = 0;
            $output["weekday"] = array();
            while (($row = mysql_fetch_assoc($result)) != null) {
                $sum += $row["playtime"];
                $output["weekday"][] = array(
                    "weekday" => $row["day"],
                    "playtime" => intval($row["playtime"])
                );
            }
            $output["sum"] = $sum;
        break;
        case "yearly":
            $query = sprintf('
                SELECT
                    sum(playtime) as playtime,
                    YEAR(time) as year
                FROM
                    hourly_playtime
                WHERE
                    steamid = %1$s
                GROUP BY
                    YEAR(time)
                ', $steamid);
            $result = mysql_query($query);
            $output["yearly"] = array();
            while (($row = mysql_fetch_assoc($result)) != null) {
                $output["yearly"][] = array(
                    "year" => $row["year"],
                    "playtime" => intval($row["playtime"])
                );
            }
        break;
    }
}

echo json_encode($output);
mysql_close();

?>
